<h2>Support channels</h2>

Join our support team at https://t.me/adiuto\_support.

<h2>Introduction</h2>

adiuto.org lists calls for donations with the required amount, urgency and location of the need. It is easy for people to find out where and what help is needed. Initiatives can post needs-based calls for donations and keep track of the donation supply chain.

Donations are called tasks and several tasks that an individual pledges to do are called a mission.

<h2>Individuals</h2>

Join adiuto.org to find donation needs in your vicinity.

<ol>
<li>Scroll through the list of tasks and use the filters on the right to narrow the list.</li>
<li>Pick a task and add it to a mission.<br>
The mission is accessible from the right menu with a link to find more tasks for the same location. (A mission can only have tasks for one location)</li>
<li>When you're done adding tasks to your mission, go to <strong>checkout</strong> (accessible form the mission view in the right menu). 
<li>Your mission is now listed under <en>my missions</em>, accessible from the right menu.</li>
<li>Click on the mission number to reveal a summery of the mission tasks and a QR Code. (Additionally You'll will also receive an Email with the same information)</li>
<li>Take your donations to the initiative and an initiative staffer can scan your QR code to update the database and <strong>complete</strong> the mission.</li>
</ol>

<h2>Initiatives</h2>

join adiuto.org to manage your donation supply chain.

<h3>Create a page for your initiatve, or ...</h3>

Go to the initiative overview page and click on the <em>register new initiative</em> button in the right menu.

<h3>... join an existing initiative</h3>

Open the page of the initiative you would like to join and use the <em>join initiative</em> link from the right menu. An <strong>administrator</strong> has to approve your request.

There are three roles for members of an initiative:

<ul>
<li><strong>Members</strong> can create tasks and manage incoming missions (scan the QR code when the donations successfully arrives). Newly created tasks are deactivated and need to be activated by a <strong>coordinator</strong>.</li>

<li><strong>Coordinator</strong> can also activate and update any tasks for their initiative.</li>

<li><strong>Administrators</strong> can also change the role of members, accept new ones and remove members form the initiative.</li>
</ul>

<h3>Managing an initiative</h3>

On the page of an initiative all links to manage tasks can be found in the right menu. From there you can create tasks and manage members. You also have an overview of the missions that are underway.

<h3>Demand</h3>

Every tasks has a demand that shows how many of those things are needed. Once a mission with that task leaves the <strong>checkout</strong>, the demand is reduced accordingly to take the donations in to accout that are underway. If a mission is <strong>canceled</strong> the original demand is restored.

<h2>Drop off</h2>

After <strong>checkout</strong> the doner collects all the needed donations and goes to the designated location to drop them of.<br>
Every mission has a QR Code that can be found along the mission details on adiuto.org (under <em>my missions</em>) or in the email that was send after the checkout process.<br>
At the Drop off point an initiative staffer can scan the QR code with thier smartphone and click the displayed link. Clicking the link <strong>completes</strong> and finishes the mission. (The initiative staffer as to be a member of the initiative on adiuto.org and be logged in.)

<h2>Contact</h2>

If you have any further questions don't hesitate to contanct the adiuto.org team <a href="mailto:info@adiuto.org">info@adiuto.org</a>.
